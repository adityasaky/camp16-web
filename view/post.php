<?php
session_start();
require '../includes/config.php';
$slug = $_GET['slug'];
$sql = "SELECT * FROM posts WHERE Slug='$slug'";
$post = mysqli_query($conn, $sql);
$post = mysqli_fetch_object($post);
?>
<?php
require '../includes/head.php';
?>
<body>
  <?php
  require '../includes/nav.php';
  ?>
  <div class="container">
    <div class="content list">
      <div class="content post">
        <h1 class="post-title"><?=$post->Title ?></h1>
        <div class="post-date">
          <time><?php

          $date = DateTime::createFromFormat('Y-m-d H:i:s', $post->Date)->format('d M Y');
          $time = DateTime::createFromFormat('Y-m-d H:i:s', $post->Date)->format('H:i');
          echo 'Posted on ' . $date . ' at ' . $time;

          ?></time>
        </div>
        <?=$post->Content ?>
      </div>

    </div>
  </div>
  <?php
  require '../includes/foot.php';
  ?>
