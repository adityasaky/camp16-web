<?php
session_start();
require '../includes/config.php';
$slug = $_GET['slug'];
$sql = "SELECT * FROM pages WHERE Slug='$slug'";
$page = mysqli_query($conn, $sql);
$page = mysqli_fetch_object($page);
?>
<?php
require '../includes/head.php';
?>
<body>
  <?php
  require '../includes/nav.php';
  ?>
  <div class="container">
    <div class="content list">
      <div class="content page">
        <h1 class="page-title"><?=$page->Title ?><div class="page-title-stop">.</div></h1>
        <?=$page->Content ?>
      </div>
    </div>
  </div>
  <?php
  require '../includes/foot.php';
  ?>
