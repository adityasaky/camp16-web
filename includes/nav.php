<?php
$sql = "SELECT * FROM pages ORDER BY Nav";
$navbar = mysqli_query($conn, $sql);
?>
<header class="masthead">
  <h1 class="masthead-title masthead-center">
    <a href="<?=$baseurl ?>/"><img src="<?=$baseurl ?>/images/Camp16Logo.svg"></a>
  </h1>
  <nav class="masthead-nav">
    <a href="<?=$baseurl ?>/updates">Updates</a>
    <?php
    while ($nav = mysqli_fetch_object($navbar)) {
      if ($nav->Nav < 0) {
        continue;
      }
      ?>
      <a href="<?=$baseurl ?>/page/<?=$nav->Slug ?>"><?=$nav->Title ?></a>
      <?php
    }
    ?>
  </nav>
</header>
