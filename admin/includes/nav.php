<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
  <div class="container">
    <!-- Create mobile toggle button and bar -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <!-- Add the horizontal bars to the toggle button -->
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=$baseurl ?>">FSMK Camps Admin</a>
    </div>
    <!-- All the links -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="<?=$baseurl ?>/view/allparticipants.php">Participants</a>
        </li>
        <li>
          <a href="<?=$baseurl ?>/view/allusers.php">Users</a>
        </li>
        <li>
          <a href="<?=$baseurl ?>/view/allvolunteers.php">Volunteers</a>
        </li>
        <li>
          <a href="<?=$baseurl ?>/view/allposts.php">Posts</a>
        </li>
        <li>
          <a href="<?=$baseurl ?>/view/allpages.php">Pages</a>
        </li>
        <li>
          <a href="<?=$baseurl ?>/action/logout.php">Log Out</a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>
