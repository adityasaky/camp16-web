<?php
session_start();
require '../includes/config.php';
if ($_SESSION['signin_check'] == 1) {
  $name = $_POST['name'];
  $name = $conn->real_escape_string($name);
  $email = $_POST['email'];
  $email = $conn->real_escape_string($email);
  $picture = $_POST['picture'];
  $picture = $conn->real_escape_string($picture);
  $picture = $picture . ".jpg";
  $phone = $_POST['phone'];
  $phone = $conn->real_escape_string($phone);
  $college = $_POST['college'];
  $college = $conn->real_escape_string($college);
  $track = $_POST['track'];
  $track = $conn->real_escape_string($track);
  $sql = "INSERT INTO volunteers (Name, Email, Picture, Phone, College, Track) values ('$name', '$email', '$picture', '$phone', '$college', '$track')";
  if (mysqli_query($conn, $sql)) {
    header("Location: $baseurl/view/allvolunteers.php");
  }
  else {
    echo "Error - contingency activated - contact admin";
  }
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
