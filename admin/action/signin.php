<?php
session_start();
require '../includes/config.php';
$username = $_POST['username'];
$username = $conn->real_escape_string($username);
$password = $_POST['password'];
$password = $conn->real_escape_string($password);
$sql = "SELECT * FROM users WHERE Username='$username'";
if ($user = mysqli_query($conn, $sql)) {
  $user = mysqli_fetch_object($user);
  if(password_verify($password, $user->Password)) {
    $_SESSION['id'] = $user->ID;
    $_SESSION['username'] = $user->Username;
    $_SESSION['name'] = $user->Name;
    $_SESSION['signin_check'] = 1;
    header("Location: $baseurl");
  }
  else {
    $_SESSION['signin_check'] = 0;
    header("Location: $baseurl/view/signin.php");
  }
}
else {
  $_SESSION['signin_check'] = 0;
  header("Location: $baseurl/view/signin.php");
}
?>
