<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>New Post</h2>
    <form name="newpost" action="../action/newpost.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="title" id="title" placeholder="Title" autofocus>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="slug" id="slug" placeholder="Slug" readonly>
      </div>
      <div class="form-group">
        <textarea class="form-control" name="content" placeholder="Content" rows=10></textarea>
      </div>
      <div class="form-group">
        <input type="submit" value="Add Post" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<script type="text/javascript">
var t1 = document.getElementById('title');
var t2 = document.getElementById('slug');

t1.addEventListener('keyup', function () { t2.value = t1.value.replace(/[^\w]+/g, '-'); }, true);

</script>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
