<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM posts WHERE ID='$id'";
    $post = mysqli_query($conn, $sql);
    $post = mysqli_fetch_object($post);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>Edit Post</h2>
    <form name="post" action="../action/post.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="title" id="title" placeholder="Title" value="<?=$post->Title ?>" autofocus>
      </div>
      <div class="form-group">
        <textarea class="form-control" name="content" placeholder="Content" rows=10><?=$post->Content ?></textarea>
      </div>
      <input type="hidden" value="<?=$id ?>" name="id">
      <div class="form-group">
        <input type="submit" value="Edit Post" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
