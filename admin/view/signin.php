<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
?>

<body>
  <div class="container">

    <h2>Sign In</h2>
    <form name="signin" action="../action/signin.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="username" placeholder="Username" autofocus>
      </div>
      <div class="form-group">
        <input class="form-control" type="password" name="password" placeholder="Enter password">
      </div>
      <div class="form-group">
        <input type="submit" value="Sign In" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<?php
  require '../includes/foot.php';
?>
