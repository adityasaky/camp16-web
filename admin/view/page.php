<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM pages WHERE ID='$id'";
    $page = mysqli_query($conn, $sql);
    $page = mysqli_fetch_object($page);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>Edit Page</h2>
    <form name="page" action="../action/page.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="title" id="title" placeholder="Title" value="<?=$page->Title ?>" autofocus>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="nav" placeholder="Nav" value="<?=$page->Nav ?>">
      </div>
      <div class="form-group">
        <textarea class="form-control" name="content" placeholder="Content" rows=10><?php echo htmlspecialchars($page->Content); ?></textarea>
      </div>
      <input type="hidden" value="<?=$id ?>" name="id">
      <div class="form-group">
        <input type="submit" value="Edit Page" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
