<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $sql = "SELECT * FROM participants WHERE Deleted=0 ORDER BY Track,Name";
    $participants = mysqli_query($conn, $sql);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>All Participants</h2>
    <a href="<?=$baseurl ?>/action/export_csv.php" class="btn btn-primary">Export</a>
    <?php
      $sql = "SELECT * FROM participants WHERE Track='Web1' AND Deleted='0'";
      $web1 = mysqli_query($conn, $sql);
      $web1 = mysqli_num_rows($web1);
      $sql = "SELECT * FROM participants WHERE Track='Web2' AND Deleted='0'";
      $web2 = mysqli_query($conn, $sql);
      $web2 = mysqli_num_rows($web2);
      $sql = "SELECT * FROM participants WHERE Track='Animation' AND Deleted='0'";
      $animation = mysqli_query($conn, $sql);
      $animation = mysqli_num_rows($animation);
      $sql = "SELECT * FROM participants WHERE Track='Hardware' AND Deleted='0'";
      $hardware = mysqli_query($conn, $sql);
      $hardware = mysqli_num_rows($hardware);
      $total = mysqli_num_rows($participants);
    ?>
    <table class="table">
      <thead>
        <th>
          Track
        </th>
        <th>
          Web Front-End
        </th>
        <th>
          Web Back-End
        </th>
        <th>
          Animation
        </th>
        <th>
          Hardware
        </th>
        <th>
          Total
        </th>
      </thead>
      <tbody>
        <tr>
          <td>
            <b>Number of Registrations</b>
          </td>
          <td>
            <?=$web1 ?>
          </td>
          <td>
            <?=$web2 ?>
          </td>
          <td>
            <?=$animation ?>
          </td>
          <td>
            <?=$hardware ?>
          </td>
          <td>
            <?=$total ?>
          </td>
        </tr>
      </tbody>
    </table>
<?php
if (mysqli_num_rows($participants)) {
 ?>
    <table class="table">
      <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Track</th>
        <th>Date</th>
        <th>View</th>
        <th>Delete</th>
      </thead>
      <tbody>
        <?php
         while ($participant = mysqli_fetch_object($participants)) {
        ?>
        <tr>
          <td><?=$participant->ID ?></td>
          <td><?=$participant->Name ?></td>
          <td><a href="mailto:<?=$participant->Email ?>"><?=$participant->Email ?></a></td>
          <td><?=$participant->Track ?></td>
          <td><?=$participant->Date ?></td>
          <td><a href="<?=$baseurl ?>/view/participant.php?id=<?=$participant->ID ?>">View</a></td>
          <td><a href="<?=$baseurl ?>/action/deleteparticipant.php?id=<?=$participant->ID ?>">Delete</a></td>
        </tr>
        <?php
      }
        ?>
    </table>
<?php
} else {
  echo "No registrations yet. Huehuehue";
}
?>
  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
